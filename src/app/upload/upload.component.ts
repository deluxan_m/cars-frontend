import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css'],
})
export class UploadComponent implements OnInit {
  afuConfig = {
    formatsAllowed: '.csv,.xlsx',
    uploadAPI: {
      url: 'http://localhost:3000/cars/upload',
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
    },
  };

  constructor() {}

  ngOnInit(): void {}
}
