import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import axios from 'axios';
import { faTrash, faPenAlt } from '@fortawesome/free-solid-svg-icons';

interface Car {
  id: number;
  first_name: string;
  last_name: string;
  email: string;
  car_make: string;
  car_model: string;
  vin: string;
  manufactured_date: Date;
  age_of_vehicle: number;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  page = 1;
  pageSize = 4;
  cars: Car[] = [];
  collectionSize = this.cars.length;

  available = false;

  faTrash = faTrash;
  faPenAlt = faPenAlt;

  constructor(private router: Router) {}

  ngOnInit(): void {
    this.refreshCountries();
  }

  async refreshCountries() {
    await axios
      .get('http://localhost:3000/cars/all')
      .then((res) => {
        this.cars = res.data.map((car: Car) => car);
        this.available = true;
        console.log(res.data);
      })
      .catch((error) => console.error(error));
  }

  async deleteCarById(id: number) {
    await axios
      .delete(`http://localhost:3000/cars/delete/${id}`)
      .then((res) => console.log(res))
      .catch((err) => console.error(err));
  }

  alert(car: Car) {
    const { id, first_name, last_name, car_make } = car;

    if (
      confirm(
        `Are you sure you want to delete ${first_name} ${last_name}'s ${car_make} from database?`
      )
    ) {
      this.deleteCarById(id);
    }
  }

  redirectToUpdate(car: Car) {
    this.router.navigate([`/update/${car.id}`]);
  }

  calculateAgeOfVehicle(dateSent: Date) {
    let currentDate = new Date();
    dateSent = new Date(dateSent);

    return Math.floor(
      (Date.UTC(
        currentDate.getFullYear(),
        currentDate.getMonth(),
        currentDate.getDate()
      ) -
        Date.UTC(
          dateSent.getFullYear(),
          dateSent.getMonth(),
          dateSent.getDate()
        )) /
        (1000 * 60 * 60 * 24)
    );
  }
}
