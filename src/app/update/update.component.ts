import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css'],
})
export class UpdateComponent implements OnInit {
  updateForm!: FormGroup;
  submitted = false;

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.updateForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      carMake: ['', Validators.required],
      carModel: ['', Validators.required],
      vin: ['', Validators.required],
      manufacturedDate: ['', Validators.required],
      ageOfVehicle: [],
    });
  }

  get h() {
    return this.updateForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    if (this.updateForm.invalid) {
      return;
    }

    console.table(this.updateForm.value);
    console.table(this.updateForm);
  }
}
